public const string[] reglist = {"%r8", "%r9", "%r10", "%r11"};
public const string[] breglist = {"%r8b", "%r9b", "%r10b", "%r11b"};
public bool[] regs;

public void freeall_register() {
       regs[0] = true; 
       regs[1] = true; 
       regs[2] = true; 
       regs[3] = true; 
}

public int alloc_register() {
        for(int i = 0; i < 4; i++) {
                if(regs[i]) {
                        regs[i] = false;
                        return i;
                }
        }
        error("No More registers");
}

public void free_register(int reg) {
        regs[reg] = true;
}

public void cgpreamble() {
        regs = {true, true, true, true};
        printint();
}

public void printint() {
        print(
                ".text\n" +
                ".LC0:\n" +
                ".string\t\"%d\\n\"\n" +
                "printint:\n" +
                "\tpushq\t%rbp\n" +
	        "\tmovq\t%rsp, %rbp\n" +
	        "\tsubq\t$16, %rsp\n" +
	        "\tmovl\t%edi, -4(%rbp)\n" +
	        "\tmovl\t-4(%rbp), %eax\n" +
	        "\tmovl\t%eax, %esi\n" +
	        "\tleaq	.LC0(%rip), %rdi\n" +
	        "\tmovl	$0, %eax\n" +
	        "\tcall	printf@PLT\n" +
	        "\tnop\n" +
	        "\tleave\n" +
	        "\tret\n" +
	        "\n"
                );
}

public void cgfunpreable(string name) {
        print("\t.text\n");
        print(@"\t.globl $name\n");
        print(@"\t.type $name, @function\n");
        print(@"$name:\n");
        print("\tpushq %rbp\n");
        print("\tmovq %rsp, %rbp\n");
}

public void cgfunpostamble() {
        print("\tmovl $0, %" + "eax\n");
        print("\tmovq %rbp, %rsp\n");
        print("\tpopq %rbp\n");
        print("\tret\n");
}

public int cgload(int val) {
        int r = alloc_register();
        print("\tmovq $" + @"$val, $(reglist[r])\n");
        return r;
}

public int cgloadsym(string name) {
        int r = alloc_register();
        print(@"\tmovq $(name)(%rip), $(reglist[r])\n");
        return r;
}

public int cgstoresym(int reg, string name) {
        print(@"\tmovq $(reglist[reg]), $(name)(%rip)\n");
        return reg;
}

public void cgprintint(int r) {
        print(@"\tmovq $(reglist[r]), %rdi\n");
        print("\tcall printint\n");
        free_register(r);
}

public int cgadd(int r1, int r2) {
        print(@"\taddq $(reglist[r1]), $(reglist[r2])\n");
        free_register(r1);
        return r2;
}

public int cgmul(int r1, int r2) {
        print(@"\timulq $(reglist[r1]), $(reglist[r2])\n");
        free_register(r1);
        return r2;
}

public int cgsub(int r1, int r2) {
        print(@"\tsubq $(reglist[r2]), $(reglist[r1])\n");
        free_register(r2);
        return r1;
}

public int cgdiv(int r1, int r2) {
        print(@"\tmovq $(reglist[r1]), %rax\n");
        print(@"\tcqo\n");
        print(@"\tidivq $(reglist[r2])\n");
        print(@"\tmovq %rax, $(reglist[r1])\n");
        free_register(r2);
        return r1;
}

public void gensym(string name) {
        print(@"\t.comm $name,8,8\n");
}

public int cgcmp(int r1, int r2, string how) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\t$(how) $(breglist[r2])\n");
        print("\tandq $255, " + @"$(reglist[r2])\n");
        free_register(r1);
        return r2;
}

public int cgequ(int r1, int r2) {
        return cgcmp(r1, r2, "sete");
}

public int cgequj(int r1, int r2, int l) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\tjne L$l\n");
        return 0;
}

public int cgnequ(int r1, int r2) {
        return cgcmp(r1, r2, "setne");
}

public int cgnequj(int r1, int r2, int l) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\tje L$l\n");
        return 0;
}

public int cgless(int r1, int r2) {
        return cgcmp(r1, r2, "setl");
}

public int cglessj(int r1, int r2, int l) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\tjge L$l\n");
        return 0;
}

public int cggrt(int r1, int r2) {
        return cgcmp(r1, r2, "setg");
}

public int cggrtj(int r1, int r2, int l) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\tjle L$l\n");
        return 0;
}

public int cgleq(int r1, int r2) {
        return cgcmp(r1, r2, "setle");
}

public int cgleqj(int r1, int r2, int l) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\tjg L$l\n");
        return 0;
}

public int cggeq(int r1, int r2) {
        return cgcmp(r1, r2, "setge");
}

public int cggeqj(int r1, int r2, int l) {
        print(@"\tcmpq $(reglist[r2]), $(reglist[r1])\n");
        print(@"\tjl L$l\n");
        return 0;
}

public void cglabel(int l) {
        print(@"L$l:\n");
}

public void cgjump(int l) {
        print(@"\tjmp L$l\n");
}

public int gen(BaseNode n, int reg) {
        if(n is BinaryOperatorNode) {
                var expr = n as BinaryOperatorNode;
                int r1 = gen(expr.left, reg);
                int r2 = gen(expr.right, reg);
                switch(expr.operator) {
                        case "+":
                                return cgadd(r1, r2);
                        case "-":
                                return cgsub(r1, r2);
                        case "*":
                                return cgmul(r1, r2);
                        case "/":
                                return cgdiv(r1, r2);
                        case "==":
                                return expr.parentIsJump ? cgequj(r1, r2, reg) : cgequ(r1, r2);
                        case "!=":
                                return expr.parentIsJump ? cgnequj(r1, r2, reg) : cgnequ(r1, r2);
                        case "<":
                                return expr.parentIsJump ? cglessj(r1, r2, reg) : cgless(r1, r2);
                        case ">":
                                return expr.parentIsJump ? cggrtj(r1, r2, reg) : cggrt(r1, r2);
                        case "<=":
                                return expr.parentIsJump ? cgleqj(r1, r2, reg) : cgleq(r1, r2);
                        case ">=":
                                return expr.parentIsJump ? cggeqj(r1, r2, reg) : cggeq(r1, r2);
                }
        } else if(n is IntLitteralNode) {
                var expr = n as IntLitteralNode;
                return cgload(expr.val);
        } else if(n is IdentifierNode) {
               var expr = n as IdentifierNode;
               return cgloadsym(expr.val);
        } else if(n is assignOperatorNode) {
                var expr = n as assignOperatorNode;
                int r = gen(expr.left, reg);
                return cgstoresym(r, expr.id);
        } else if(n is ReturnNode) {
                var expr = n as ReturnNode;
                int r = gen(expr.expr, reg);
                cgprintint(r);
        } else if(n is BlockNode) {
                var expr = n as BlockNode;
                foreach(var st in expr.getNodes()) {
                        gen(st, reg);
                        freeall_register();
                }
        } else if(n is IfNode) {
                var expr = n as IfNode;
                int falseLabel = getNextLabel();
                int endLabel = 0;
                if(expr.elseBlock != null)
                        endLabel = getNextLabel();
                gen(expr.cond, falseLabel);
                freeall_register();
                gen(expr.ifBlock, 0);
                freeall_register();
                if(expr.elseBlock != null)
                        cgjump(endLabel);
                cglabel(falseLabel);
                if(expr.elseBlock != null) {
                        gen(expr.elseBlock, 0);
                        freeall_register();
                        cglabel(endLabel);
                }
        } else if(n is WhileNode) {
                var expr = n as WhileNode;
                int startLabel = getNextLabel();
                int endLabel = getNextLabel();
                cglabel(startLabel);
                gen(expr.cond, endLabel);
                freeall_register();
                gen(expr.block, 0);
                freeall_register();
                cgjump(startLabel);
                cglabel(endLabel);
        } else if(n is ForNode) {
                var expr = n as ForNode;
                gen(expr.preop, 0);
                freeall_register();
                int startLabel = getNextLabel();
                int endLabel = getNextLabel();
                cglabel(startLabel);
                gen(expr.cond, endLabel);
                freeall_register();
                gen(expr.block, 0);
                freeall_register();
                gen(expr.postop, 0);
                freeall_register();
                cgjump(startLabel);
                cglabel(endLabel);
        } else if(n is FunctionNode) {
                var expr = n as FunctionNode;
                cgfunpreable(expr.name);
                gen(expr.block, 0);
                cgfunpostamble();
        } else {
                error("unreachable\n");
        }
        return 0;
}
