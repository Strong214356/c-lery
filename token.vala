public enum UniformLiteral {
        INT, STR, CHAR, BOOL, FLOAT, NONE,
}

public enum TokenType {
        UNDEFINED,
        IDENTIFIER,
        KW_NAMESPACE, KW_DEFINE, KW_USE, KW_CLASS, KW_STRUCT, KW_ENUM, KW_UNION, KW_TYPE, KW_INLINE, KW_FUNCTION, KW_METHOD,
        KW_LAMBDA, KW_PROPERTY, KW_REF, KW_NEW, KW_VAR, KW_CONST, KW_RETURN, KW_DEFER, KW_SELF, KW_SET, KW_GET, KW_DEFAULT,
        KW_IF, KW_ASM, KW_ELSE, KW_ELIF, KW_SWITCH, KW_CASE, KW_GOTO, KW_BREAK, KW_CONTINUE, KW_WHILE, KW_FOR, KW_DO, KW_POLYMORPH,
        KW_EXTERN, KW_ALIAS,
        TYPE_INT8, TYPE_INT16, TYPE_INT32, TYPE_INT64, TYPE_UINT8, TYPE_UINT16, TYPE_UINT32, TYPE_UINT64, TYPE_F32,
        TYPE_F64, TYPE_F128, TYPE_BOOL, TYPE_CHAR, TYPE_STR, TYPE_VA_LIST, TYPE_VOID,
        LITTERAL_UINT, LITTERAL_FLOAT, LITTERAL_BOOL, LITTERAL_CHAR, LITTERAL_STR,
        BRACKET_OPEN, BRACKET_CLOSE, PARENTESIS_OPEN, PARENTESIS_CLOSE, SQUARE_BRACKET_OPEN, SQUARE_BRACKET_CLOSE,
        CHEVRON_OPEN, CHEVRON_CLOSE,
        OPERATOR_DIVIDE, OPERATOR_DIVIDE_EQ, OPERATOR_MULT, OPERATOR_MULT_EQ, OPERATOR_PLUS, OPERATOR_PLUS_EQ, OPERATOR_INC,
        OPERATOR_MINUS, OPERATOR_MINUS_EQ, OPERATOR_DEC, OPERATOR_MOD, OPERATOR_MOD_EQ, OPERATOR_NOT, OPERATOR_AND,
        OPERATOR_BAND, OPERATOR_BAND_EQ, OPERATOR_BNOT, OPERATOR_BNOT_EQ, OPERATOR_OR, OPERATOR_BOR, OPERATOR_BOR_EQ,
        OPERATOR_XOR, OPERATOR_BXOR, OPERATOR_BXOR_EQ, OPERATOR_NOT_EQ,OPERATOR_EQU, OPERATOR_ASSIGN, OPERATOR_GEQU,
        OPERATOR_LEQU, OPRATOR_LESS, OPERATOR_SHL, OPERATOR_SHL_EQU, OPERATOR_GREATER, OPERATOR_SHR, OPERATOR_SHR_EQU,
        OPERATOR_ACCESS, OPERATOR_NAME_ACCESS, OPERATOR_CAST,
        SPECIAL_RETURN_TYPE, SPECIAL_STRING_CAT, SPECIAL_COMA, SPECIAL_SEMICOLON, SPECIAL_VAR_TYPE,
        EOC;

        public unowned string to_string() {
                switch(this) {
                        case UNDEFINED:
                                return "UNDEFINED";
                        case IDENTIFIER:
                                return "IDENTIFIER";
                        case KW_NAMESPACE:
                                return "KW_NAMESPACE";
                        case KW_DEFINE:
                                return "KW_DEFINE";
                        case KW_USE:
                                return "KW_USE";
                        case KW_CLASS:
                                return "KW_CLASS";
                        case KW_STRUCT:
                                return "KW_STRUCT";
                        case KW_ENUM:
                                return "KW_ENUM";
                        case KW_UNION:
                                return "KW_UNION";
                        case KW_TYPE:
                                return "KW_TYPE";
                        case KW_INLINE:
                                return "KW_INLINE";
                        case KW_FUNCTION:
                                return "KW_FUNCTION";
                        case KW_METHOD:
                                return "KW_METHOD";
                        case KW_LAMBDA:
                                return "KW_LAMBDA";
                        case KW_PROPERTY:
                                return "KW_PROPERTY";
                        case KW_REF:
                                return "KW_REF";
                        case KW_NEW:
                                return "KW_NEW";
                        case KW_VAR:
                                return "KW_VAR";
                        case KW_CONST:
                                return "KW_CONST";
                        case KW_RETURN:
                                return "KW_RETURN";
                        case KW_DEFER:
                                return "KW_DEFER";
                        case KW_SELF:
                                return "KW_SELF";
                        case KW_SET:
                                return "KW_SET";
                        case KW_GET:
                                return "KW_GET";
                        case KW_DEFAULT:
                                return "KW_DEFAULT";
                        case KW_IF:
                                return "KW_IF";
                        case KW_ASM:
                                return "KW_ASM";
                        case KW_ELSE:
                                return "KW_ELSE";
                        case KW_ELIF:
                                return "KW_ELIF";
                        case KW_SWITCH:
                                return "KW_SWITCH";
                        case KW_CASE:
                                return "KW_CASE";
                        case KW_GOTO:
                                return "KW_GOTO";
                        case KW_BREAK:
                                return "KW_BREAK";
                        case KW_CONTINUE:
                                return "KW_CONTINUE";
                        case KW_WHILE:
                                return "KW_WHILE";
                        case KW_FOR:
                                return "KW_FOR";
                        case KW_DO:
                                return "KW_DO";
                        case KW_POLYMORPH:
                                return "KW_POLYMORPH";
                        case KW_EXTERN:
                                return "KW_EXTERN";
                        case KW_ALIAS:
                                return "KW_ALIAS";
                        case TYPE_INT8:
                                return "TYPE_INT8";
                        case TYPE_INT16:
                                return "TYPE_INT16";
                        case TYPE_INT32:
                                return "TYPE_INT32";
                        case TYPE_INT64:
                                return "TYPE_INT64";
                        case TYPE_UINT8:
                                return "TYPE_UINT8";
                        case TYPE_UINT16:
                                return "TYPE_UINT16";
                        case TYPE_UINT32:
                                return "TYPE_UINT32";
                        case TYPE_UINT64:
                                return "TYPE_UINT64";
                        case TYPE_F32:
                                return "TYPE_F32";
                        case TYPE_F64:
                                return "TYPE_F64";
                        case TYPE_F128:
                                return "TYPE_F128";
                        case TYPE_BOOL:
                                return "TYPE_BOOL";
                        case TYPE_CHAR:
                                return "TYPE_CHAR";
                        case TYPE_STR:
                                return "TYPE_STR";
                        case TYPE_VA_LIST:
                                return "TYPE_VA_LIST";
                        case TYPE_VOID:
                                return "TYPE_VOID";
                        case LITTERAL_UINT:
                                return "LITTERAL_UINT";
                        case LITTERAL_FLOAT:
                                return "LITTERAL_FLOAT";
                        case LITTERAL_BOOL:
                                return "LITTERAL_BOOL";
                        case LITTERAL_CHAR:
                                return "LITTERAL_CHAR";
                        case LITTERAL_STR:
                                return "LITTERAL_STR";
                        case BRACKET_OPEN:
                                return "BRACKET_OPEN";
                        case BRACKET_CLOSE:
                                return "BRACKET_CLOSE";
                        case PARENTESIS_OPEN:
                                return "PARENTESIS_OPEN";
                        case PARENTESIS_CLOSE:
                                return "PARENTESIS_CLOSE";
                        case SQUARE_BRACKET_OPEN:
                                return "SQUARE_BRACKET_OPEN";
                        case SQUARE_BRACKET_CLOSE:
                                return "SQUARE_BRACKET_CLOSE";
                        case CHEVRON_OPEN:
                                return "CHEVRON_OPEN";
                        case CHEVRON_CLOSE:
                                return "CHEVRON_CLOSE";
                        case OPERATOR_DIVIDE:
                                return "OPERATOR_DIVIDE";
                        case OPERATOR_DIVIDE_EQ:
                                return "OPERATOR_DIVIDE_EQ";
                        case OPERATOR_MULT:
                                return "OPERATOR_MULT";
                        case OPERATOR_MULT_EQ:
                                return "OPERATOR_MULT_EQ";
                        case OPERATOR_PLUS:
                                return "OPERATOR_PLUS";
                        case OPERATOR_PLUS_EQ:
                                return "OPERATOR_PLUS_EQ";
                        case OPERATOR_INC:
                                return "OPERATOR_INC";
                        case OPERATOR_MINUS:
                                return "OPERATOR_MINUS";
                        case OPERATOR_MINUS_EQ:
                                return "OPERATOR_MINUS_EQ";
                        case OPERATOR_DEC:
                                return "OPERATOR_DEC";
                        case OPERATOR_MOD:
                                return "OPERATOR_MOD";
                        case OPERATOR_MOD_EQ:
                                return "OPERATOR_MOD_EQ";
                        case OPERATOR_NOT:
                                return "OPERATOR_NOT";
                        case OPERATOR_AND:
                                return "OPERATOR_AND";
                        case OPERATOR_BAND:
                                return "OPERATOR_BAND";
                        case OPERATOR_BAND_EQ:
                                return "OPERATOR_BAND_EQ";
                        case OPERATOR_BNOT:
                                return "OPERATOR_BNOT";
                        case OPERATOR_BNOT_EQ:
                                return "OPERATOR_BNOT_EQ";
                        case OPERATOR_OR:
                                return "OPERATOR_OR";
                        case OPERATOR_BOR:
                                return "OPERATOR_BOR";
                        case OPERATOR_BOR_EQ:
                                return "OPERATOR_BOR_EQ";
                        case OPERATOR_XOR:
                                return "OPERATOR_XOR";
                        case OPERATOR_BXOR:
                                return "OPERATOR_BXOR";
                        case OPERATOR_BXOR_EQ:
                                return "OPERATOR_BXOR_EQ";
                        case OPERATOR_NOT_EQ:
                                return "OPERATOR_NOT_EQ";
                        case OPERATOR_EQU:
                                return "OPERATOR_EQU";
                        case OPERATOR_ASSIGN:
                                return "OPERATOR_ASSIGN";
                        case OPERATOR_GEQU:
                                return "OPERATOR_GEQU";
                        case OPERATOR_LEQU:
                                return "OPERATOR_LEQU";
                        case OPRATOR_LESS:
                                return "OPRATOR_LESS";
                        case OPERATOR_SHL:
                                return "OPERATOR_SHL";
                        case OPERATOR_SHL_EQU:
                                return "OPERATOR_SHL_EQU";
                        case OPERATOR_GREATER:
                                return "OPERATOR_GREATER";
                        case OPERATOR_SHR:
                                return "OPERATOR_SHR";
                        case OPERATOR_SHR_EQU:
                                return "OPERATOR_SHR_EQU";
                        case OPERATOR_ACCESS:
                                return "OPERATOR_ACCESS";
                        case OPERATOR_NAME_ACCESS:
                                return "OPERATOR_NAME_ACCESS";
                        case OPERATOR_CAST:
                                return "OPERATOR_CAST";
                        case SPECIAL_RETURN_TYPE:
                                return "SPECIAL_RETURN_TYPE";
                        case SPECIAL_STRING_CAT:
                                return "SPECIAL_STRING_CAT";
                        case SPECIAL_COMA:
                                return "SPECIAL_COMA";
                        case SPECIAL_SEMICOLON:
                                return "SPECIAL_SEMICOLON";
                        case SPECIAL_VAR_TYPE:
                                return "SPECIAL_VAR_TYPE";
                        case EOC:
                                return "EOC";
                        //default:
                        //        return "unreachable";
                }
                return "unreachable";
        }

        public unowned string operator() {
                switch (this) {
                        case OPERATOR_PLUS:
                                return "+";
                        case OPERATOR_MINUS:
                                return "-";
                        case OPERATOR_MULT:
                                return "*";
                        case OPERATOR_DIVIDE:
                                return "/";
                        case OPERATOR_MOD:
                                return "%";
                        case OPERATOR_INC:
                                return "++";
                        case OPERATOR_DEC:
                                return "--";
                        case OPERATOR_ASSIGN:
                                return "=";
                        case OPERATOR_PLUS_EQ:
                                return "+=";
                        case OPERATOR_MINUS_EQ:
                                return "-=";
                        case OPERATOR_MULT_EQ:
                                return "*=";
                        case OPERATOR_DIVIDE_EQ:
                                return "/=";
                        case OPERATOR_MOD_EQ:
                                return "%=";
                        case OPERATOR_AND:
                                return "&&";
                        case OPERATOR_OR:
                                return "||";
                        case OPERATOR_NOT:
                                return "!";
                        case OPERATOR_EQU:
                                return "==";
                        case OPERATOR_NOT_EQ:
                                return "!=";
                        case OPERATOR_GEQU:
                                return ">=";
                        case OPERATOR_LEQU:
                                return "<=";
                        case OPERATOR_GREATER:
                                return ">";
                        case OPRATOR_LESS:
                                return "<";
                        default:
                                return "";
                }
        }

        public unowned string type() {
                switch (this) {
                        case TYPE_BOOL:
                                return "bool";
                        case TYPE_INT8:
                                return "int8";
                        case TYPE_INT16:
                                return "int16";
                        case TYPE_INT32:
                                return "int32";
                        case TYPE_INT64:
                                return "int64";
                        case TYPE_UINT8:
                                return "uint8";
                        case TYPE_UINT16:
                                return "uint16";
                        case TYPE_UINT32:
                                return "uint32";
                        case TYPE_UINT64:
                                return "uint64";
                        case TYPE_F32:
                                return "f32";
                        case TYPE_F64:
                                return "f64";
                        case TYPE_F128:
                                return "f128";
                        case TYPE_STR:
                                return "str";
                        case TYPE_CHAR:
                                return "char";
                        case TYPE_VOID:
                                return "void";
                        default:
                                return "";
                }
        }

        public unowned string typeCollapse() {
                switch(this) {
                        case TYPE_INT8:
                        case TYPE_INT16:
                        case TYPE_INT32:
                        case TYPE_INT64:
                        case TYPE_UINT8:
                        case TYPE_UINT16:
                        case TYPE_UINT32:
                        case TYPE_UINT64:
                                return "num";
                        case TYPE_BOOL:
                                return "bool";
                        case TYPE_F32:
                        case TYPE_F64:
                        case TYPE_F128:
                                return "float";
                        case TYPE_CHAR:
                        case TYPE_STR:
                                return "str";
                        case TYPE_VOID:
                                return "void";
                        default:
                                (new Logger()).error("Invalid type");
                }
        }

        public UniformLiteral getUniformLitteral() {
                switch (this) {
                case LITTERAL_FLOAT:
                        return UniformLiteral.FLOAT;
                case LITTERAL_UINT:
                        return UniformLiteral.INT;
                case LITTERAL_STR:
                        return UniformLiteral.STR;
                case LITTERAL_CHAR:
                        return UniformLiteral.CHAR;
                case LITTERAL_BOOL:
                        return UniformLiteral.BOOL;
                default:
                        return UniformLiteral.NONE;
                }
        }
}

public struct TokenInfo {
        public int    line;
        public int    column;
        public string file;

        public TokenInfo(int line, int column, string file) {
                this.line = line;
                this.column = column;
                this.file = file;
        }
        
        public string to_string() {
                var sb = new StringBuilder();
                sb.append("[\n");
                sb.append(@"\tLine:   $(this.line)\n");
                sb.append(@"\tColumn: $(this.column)\n");
                sb.append(@"\tFile:   $(this.file)\n");
                sb.append("]\n");
                return (owned)sb.str;
        }
}

public struct Token {
        public string?   text;
        public TokenType type;
        public TokenInfo info;

        public Token(TokenType type, TokenInfo info, string? text = null) {
                this.info = info;
                this.type = type;
                this.text = text;
        }

        public string to_string_simple() {
                var s = @"$(this.type)";
                if(this.text != null) s += @"($(this.text ?? "unreachable"))";
                return s + "\n";
        }

        public string to_string() {
                var sb = new StringBuilder();
                sb.append("[\n");
                if(this.text != null) sb.append(@"\tToken: \"$(this.text)\"\n");
                sb.append(@"\tType:  $(this.type)\n");
                sb.append(@"\tInfo:\n$(this.info)\n");
                sb.append("]\n");
                return (owned)sb.str;
        }
}

