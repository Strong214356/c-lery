public TypeNode EntryReturnType;

public enum SyntaxType {
        NUMBER_LITTERAL,
        CHAR_LITTERAL,
        STR_LITTERAL,
        IDENTIFIER,
        TYPE,
        VAR_DECL_EXPRESSION,
        TYPE_EXPRESSION,
        BINARY_OPERATOR,
        PARENTHESIZED_EXPRESSION,
        PREFIX_EXPRESSION,
        SUFFIX_EXPRESSION,
        BLOCK_EXPRESSION,
        FUNCTION_EXPRESSION,
        FUNCTION_ALIAS_EXPRESSION,
        EXTERN_EXPRESSION,
        FUNCTION_CALL_EXPRESSION,
        RETURN_EXPRESSION,
        IF_EXPRESSION,
        WHILE_EXPRESSION,
        LABEL_EXPRESSION,
        GOTO_EXPRESSION,
        ARRAY_ACCESS,
}

public class SyntaxNode {
       public SyntaxType type; 
}

public class ExpressionNode : SyntaxNode {

}

public class NumberLitteralNode : ExpressionNode {
        public Token numberToken;

        public NumberLitteralNode(Token tok) {
                this.type = SyntaxType.NUMBER_LITTERAL;
                this.numberToken = tok;
        }

        public string to_string() {
                if(this == null) return "unknown";
                return "number Litteral :" + this.numberToken.to_string();
        }
}

public class CharLitteralNode : ExpressionNode {
        public Token numberToken;

        public CharLitteralNode(Token tok) {
                this.type = SyntaxType.CHAR_LITTERAL;
                this.numberToken = tok;
        }
}

public class StringLitteralNode : ExpressionNode {
        public Token strToken;

        public StringLitteralNode(Token tok) {
                this.type = SyntaxType.STR_LITTERAL;
                this.strToken = tok;
        }
}

public class IdentifierNode : ExpressionNode {
        public Token id;

        public IdentifierNode(Token tok) {
                this.type = SyntaxType.IDENTIFIER;
                this.id = tok;
                this.id.text = this.id.text;
        }
}

public class TypeBaseNode : ExpressionNode {
        public Token id;

        public TypeBaseNode(Token tok) {
                this.type = SyntaxType.TYPE;
                this.id = tok;
        }
}

public class TypeNode : ExpressionNode {
        public bool isRef;
        public ExpressionNode id;
        public int tableDepth;

        public TypeNode(bool isRef, ExpressionNode id, int tableDepth) {
                this.type = SyntaxType.TYPE_EXPRESSION;
                this.isRef = isRef;
                this.id = id;
                this.tableDepth = tableDepth;
        }
}

public class VarDeclNode: ExpressionNode {
        public IdentifierNode id;
        public TypeNode vtype;
        public VarDeclNode(IdentifierNode id, TypeNode type) {
                this.type = SyntaxType.VAR_DECL_EXPRESSION;
                this.id = id;
                this.vtype = type;
        }
}

public class BinaryOperationNode : ExpressionNode {
        public Token operation;
        public ExpressionNode left;
        public ExpressionNode right;

        public BinaryOperationNode(ExpressionNode left, Token tok, ExpressionNode right) {
                this.type = SyntaxType.BINARY_OPERATOR;
                this.operation = tok;
                this.left = left;
                this.right = right;
        }
}

public class ParenthesisNode : ExpressionNode {
        public ExpressionNode expr;
        public ParenthesisNode(ExpressionNode expr) {
                this.type = SyntaxType.PARENTHESIZED_EXPRESSION;
                this.expr = expr;
        }
}

public class ReturnNode : ExpressionNode {
        public ExpressionNode expr;
        public ReturnNode(ExpressionNode expr) {
                this.type = SyntaxType.RETURN_EXPRESSION;
                this.expr = expr;
        }
}

public class PrefixNode : ExpressionNode {
        public Token operator;
        public ExpressionNode expr;
        public PrefixNode(Token operator, ExpressionNode expr) {
                this.type = SyntaxType.PREFIX_EXPRESSION;
                this.operator = operator;
                this.expr = expr;
        }
}

public class SuffixNode : ExpressionNode {
        public Token operator;
        public ExpressionNode expr;
        public SuffixNode(Token operator, ExpressionNode expr) {
                this.type = SyntaxType.SUFFIX_EXPRESSION;
                this.operator = operator;
                this.expr = expr;
        }
}

public class ArrayAccessNode : ExpressionNode {
        public ExpressionNode index;
        public ExpressionNode array;
        public ArrayAccessNode(ExpressionNode array, ExpressionNode index) {
                this.array = array;
                this.index = index;
        }
}

public class LabelNode : ExpressionNode {
        public ExpressionNode expr;
        public LabelNode(ExpressionNode expr) {
                this.type = SyntaxType.LABEL_EXPRESSION;
                this.expr = expr;
        }
}

public class GotoNode : ExpressionNode {
        public ExpressionNode expr;
        public GotoNode(ExpressionNode expr) {
                this.type = SyntaxType.GOTO_EXPRESSION;
                this.expr = expr;
        }
}

public class BlockNode : ExpressionNode {
        public ExpressionNode[] expressions;

        public BlockNode(ExpressionNode[] expressions) {
                this.type = SyntaxType.BLOCK_EXPRESSION;
                this.expressions = expressions;
        }
}

public class FunctionNode : ExpressionNode {
        public ExpressionNode id;
        public ExpressionNode[] args;
        public BlockNode body;
        public ExpressionNode returnType;

        public FunctionNode(ExpressionNode id, ExpressionNode[] args, BlockNode body, ExpressionNode returnType) {
                this.type = SyntaxType.FUNCTION_EXPRESSION;
                this.id = id;
                this.args = args;
                this.body = body;
                this.returnType = returnType;
        }
}

public class FunctionAliasNode : ExpressionNode {
        public ExpressionNode id;
        public ExpressionNode alias;

        public FunctionAliasNode(ExpressionNode id, ExpressionNode alias) {
                this.type = SyntaxType.FUNCTION_ALIAS_EXPRESSION;
                this.id = id;
                this.alias = alias;
        }
}

public class ExternNode : ExpressionNode {
        public ExpressionNode id;
        public ExpressionNode[] args;
        public ExpressionNode returnType;

        public ExternNode(ExpressionNode id, ExpressionNode[] args, ExpressionNode returnType) {
                this.type = SyntaxType.EXTERN_EXPRESSION;
                this.id = id;
                this.args = args;
                this.returnType = returnType;
        }
}

public class FunctionCallNode : ExpressionNode {
        public ExpressionNode id;
        public ExpressionNode[] args;

        public FunctionCallNode(ExpressionNode id, ExpressionNode[] args) {
                this.type = SyntaxType.FUNCTION_CALL_EXPRESSION;
                this.id = id;
                this.args = args;
        }
}

public class IfNode : ExpressionNode {
        public ExpressionNode condition;
        public BlockNode ifBody;
        public BlockNode? elseBody;

        public IfNode(ExpressionNode condition, BlockNode ifBody, BlockNode? elseBody = null) {
                this.type = SyntaxType.IF_EXPRESSION;
                this.condition = condition;
                this.ifBody = ifBody;
                this.elseBody = elseBody;
        }
}

public class WhileNode : ExpressionNode {
        public ExpressionNode condition;
        public BlockNode body;

        public WhileNode(ExpressionNode condition, BlockNode body) {
                this.type = SyntaxType.WHILE_EXPRESSION;
                this.condition = condition;
                this.body = body;
        }
}

public class SyntaxTree {
        public ExpressionNode[] root;
        public Token EOC;

        public SyntaxTree(ExpressionNode[] root, Token endOfCode) {
                this.root = root;
                this.EOC = endOfCode;
        }
}

public class Parser {
        Token[] toks;
        Logger _log;
        int pos;

        public Parser(Token[] toks) {
                this.toks = toks;
                this.pos = 0;
                this._log = new Logger("Parser");
        }

        public Token peek(int offset = 1) {
                var index = this.pos + offset;
                if(index >= this.toks.length)
                        return this.toks[this.toks.length - 1];
                return this.toks[index];
        }

        public Token next() {
                var ret = this.toks[this.pos];
                this.pos++;
                return ret;
        } 

        public Token? match(TokenType type) {
                if(peek(0).type == type)
                        return next();
                return null;
        }

        public SyntaxTree parse() {
                ExpressionNode[] program = {};
                while(peek(0).type != TokenType.EOC) {
                        var expr = parseGlobalExpression();
                        program += expr;
                }
                var tree = new SyntaxTree(program, peek(0));
                return tree;
        }

        public ExpressionNode parseGlobalExpression() {
                var current = peek(0);
                ExpressionNode node;

                switch(current.type) {
                        case TokenType.KW_FUNCTION:
                                node = parseFunctionExpression();
                                break;
                        case TokenType.KW_EXTERN:
                                node = parseExternExpression();
                                current = peek(0);
                                if(current.type != TokenType.SPECIAL_SEMICOLON) _log.TokenError(current.info, @"Unexpected token1 $(current.type)");
                                next();
                                break;
                        case TokenType.KW_ALIAS:
                                node = parseAliasExpression();
                                current = peek(0);
                                if(current.type != TokenType.SPECIAL_SEMICOLON) _log.TokenError(current.info, @"Unexpected token2 $(current.type)");
                                next();
                                break;
                        default:
                                _log.TokenError(current.info, @"Unexpected token3 $(current.type)");
                }
                return node;
        }

        public ExpressionNode parseAliasExpression() {
                var current = peek(0);
                ExpressionNode node;
                if(current.type != TokenType.KW_ALIAS) _log.TokenError(current.info, @"Unexpected token4 $(current.type)");
                next();
                current = peek(0);
                switch(current.type) {
                        case TokenType.KW_FUNCTION:
                                next();
                                var id = parsePrimayExpression();
                                current = peek(0);
                                if(current.type != TokenType.SPECIAL_RETURN_TYPE) _log.TokenError(current.info, @"Unexpected token5 $(current.type)");
                                next();
                                var alias = parsePrimayExpression();
                                node = new FunctionAliasNode(id, alias);
                                break;
                        default:
                                _log.TokenError(current.info, @"Unable to alias from token $(current.type)");
                }
                return node;
        }

        public ExpressionNode parseExternExpression() {
                var current = peek(0);
                ExpressionNode node;
                if(current.type != TokenType.KW_EXTERN) _log.TokenError(current.info, @"Unexpected token6 $(current.type)");
                next();
                current = peek(0);
                if(current.type != TokenType.IDENTIFIER) _log.TokenError(current.info, @"Unexpected token7 $(current.type)");
                var id = parsePrimayExpression() as IdentifierNode;
                current = peek(0);
                if(current.type != TokenType.PARENTESIS_OPEN) _log.TokenError(current.info, @"Unexpected token8 $(current.type)");
                next();
                var args = parseExternArgs();
                current = peek(0);
                if(current.type != TokenType.PARENTESIS_CLOSE) _log.TokenError(current.info, @"Unexpected token9 $(current.type)");
                next();
                current = peek(0);
                if(current.type != TokenType.SPECIAL_RETURN_TYPE) _log.TokenError(current.info, @"Unexpected token10 $(current.type)");
                next();
                var returnType = parseTypeExpression();
                node = new ExternNode(id, args, returnType);
                return node;
        }

        public ExpressionNode parseFunctionExpression() {
                var current = peek(0);
                ExpressionNode node;
                if(current.type != TokenType.KW_FUNCTION) _log.TokenError(current.info, @"Unexpected token11 $(current.type)");
                next();
                current = peek(0);
                if(current.type != TokenType.IDENTIFIER) _log.TokenError(current.info, @"Unexpected token12 $(current.type)");
                var id = parsePrimayExpression() as IdentifierNode;
                current = peek(0);
                if(current.type != TokenType.PARENTESIS_OPEN) _log.TokenError(current.info, @"Unexpected token13 $(current.type)");
                next();
                var args = parseFunctionArgs();
                current = peek(0);
                if(current.type != TokenType.PARENTESIS_CLOSE) _log.TokenError(current.info, @"Unexpected token14 $(current.type)");
                next();
                current = peek(0);
                if(current.type != TokenType.SPECIAL_RETURN_TYPE) _log.TokenError(current.info, @"Unexpected token15 $(current.type)");
                next();
                var returnType = parseTypeExpression();
                if(id.id.text == "entry") EntryReturnType = returnType as TypeNode;
                current = peek(0);
                if(current.type != TokenType.BRACKET_OPEN) _log.TokenError(current.info, @"Unexpected token16 $(current.type)");
                next();
                var body = parseBlockExpression();
                current = peek(0);
                if(current.type != TokenType.BRACKET_CLOSE) _log.TokenError(current.info, @"Unexpected token17 $(current.type)");
                next();
                node = new FunctionNode(id, args, body, returnType);
                return node;
        }

        public BlockNode parseBlockExpression() {
                var current = peek(0);
                ExpressionNode[] expressions = {};
                while(true) {
                        current = peek(0);
                        if(current.type == TokenType.BRACKET_CLOSE) break;
                        var expr = parseExpression();
                        expressions += expr;
                        current = peek(0);
                        if(current.type == TokenType.SPECIAL_SEMICOLON
                           || expressions[expressions.length - 1].type == SyntaxType.IF_EXPRESSION
                           || expressions[expressions.length - 1].type == SyntaxType.WHILE_EXPRESSION) {
                                if(current.type == TokenType.SPECIAL_SEMICOLON) next();
                                continue;
                        }
                        break;
                }
                return new BlockNode(expressions);
        }

        public ExpressionNode[] parseFunctionArgs() {
                var current = peek(0);
                var args = new ExpressionNode[] {};
                if(current.type == TokenType.PARENTESIS_CLOSE) return args;
                while(true) {
                        var arg = parseParameter();
                        args += arg;
                        current = peek(0);
                        if(current.type == TokenType.SPECIAL_COMA) {
                                next();
                                continue;
                        }
                        break;
                }
                return args;
        }

        public ExpressionNode[] parseExternArgs() {
                var current = peek(0);
                var args = new ExpressionNode[] {};
                if(current.type == TokenType.PARENTESIS_CLOSE) return args;
                while(true) {
                        var arg = parseTypeExpression();
                        args += arg;
                        current = peek(0);
                        if(current.type == TokenType.SPECIAL_COMA) {
                                next();
                                continue;
                        }
                        break;
                }
                return args;
        }

        public ExpressionNode parseParameter() {
                var current = peek(0);
                ExpressionNode node;
                if(current.type != TokenType.IDENTIFIER) _log.TokenError(current.info, @"Unexpected token18 $(current.type)");
                var id = parsePrimayExpression() as IdentifierNode;
                current = peek(0);
                if(current.type != TokenType.SPECIAL_VAR_TYPE) _log.TokenError(current.info, @"Unexpected token19 $(current.type)");
                next();
                var type = parseTypeExpression();
                node = new VarDeclNode(id, type);
                return node;
        }

        public ExpressionNode parseExpression() {
                var current = peek(0);
                ExpressionNode expr;

                switch(current.type) {
                        case TokenType.KW_RETURN:
                                next();
                                expr = parseRank1Expression();
                                expr = new ReturnNode(expr);
                                break;
                        case TokenType.OPERATOR_ACCESS:
                                next();
                                expr = parsePrimayExpression();
                                expr = new LabelNode(expr);
                                break;
                        case TokenType.KW_GOTO:
                                next();
                                expr = parsePrimayExpression();
                                expr = new GotoNode(expr);
                                break;
                        case TokenType.KW_IF:
                                next();
                                current = peek(0);
                                if(current.type != TokenType.PARENTESIS_OPEN) _log.TokenError(current.info, @"Unexpescted token $(current.type)");
                                next();
                                var cond = parseConditionExpression();
                                current = peek(0);
                                if(current.type != TokenType.PARENTESIS_CLOSE) _log.TokenError(current.info, @"Unexpaected token $(current.type)");
                                next();
                                current = peek(0);
                                if(current.type != TokenType.BRACKET_OPEN) _log.TokenError(current.info, @"Unexpecteid token $(current.type)");
                                next();
                                var ifBody = parseBlockExpression();
                                current = peek(0);
                                if(current.type != TokenType.BRACKET_CLOSE) _log.TokenError(current.info, @"Unexpecteid token $(current.type)");
                                next();
                                current = peek(0);
                                if(current.type != TokenType.KW_ELSE) {
                                        expr = new IfNode(cond, ifBody);
                                        break;
                                }
                                next();
                                current = peek(0);
                                if(current.type != TokenType.BRACKET_OPEN) _log.TokenError(current.info, @"Unexpecteid token $(current.type)");
                                next();
                                var elseBody = parseBlockExpression();
                                current = peek(0);
                                if(current.type != TokenType.BRACKET_CLOSE) _log.TokenError(current.info, @"Unexpecteid token $(current.type)");
                                next();
                                expr = new IfNode(cond, ifBody, elseBody);
                                break;
                        case TokenType.IDENTIFIER:
                                switch(peek(1).type) {
                                        case TokenType.OPERATOR_PLUS_EQ:
                                        case TokenType.OPERATOR_MINUS_EQ:
                                        case TokenType.OPERATOR_MULT_EQ:
                                        case TokenType.OPERATOR_DIVIDE_EQ:
                                        case TokenType.OPERATOR_MOD_EQ:
                                        case TokenType.OPERATOR_ASSIGN:
                                                expr = parsePrimayExpression();
                                                expr = parseRankAssignExpression(expr);
                                                break;
                                        case TokenType.SPECIAL_VAR_TYPE:
                                                expr = parsePrimayExpression();
                                                next();
                                                var type = parseTypeExpression();
                                                expr = new VarDeclNode(expr as IdentifierNode, type);
                                                expr = parseRankAssignExpression(expr);
                                                break;
                                        default:
                                                expr = parseRank1Expression();
                                                expr = parseRankAssignExpression(expr);
                                                break;
                                }
                                break;
                        case TokenType.KW_WHILE:
                                next();
                                current = peek(0);
                                if(current.type != TokenType.PARENTESIS_OPEN) _log.TokenError(current.info, @"Unexpescted token $(current.type)");
                                next();
                                var cond = parseConditionExpression();
                                current = peek(0);
                                if(current.type != TokenType.PARENTESIS_CLOSE) _log.TokenError(current.info, @"Unexpaected token $(current.type)");
                                next();
                                current = peek(0);
                                if(current.type != TokenType.BRACKET_OPEN) _log.TokenError(current.info, @"Unexpecteid token $(current.type)");
                                next();
                                var body = parseBlockExpression();
                                current = peek(0);
                                if(current.type != TokenType.BRACKET_CLOSE) _log.TokenError(current.info, @"Unexpecteid token $(current.type)");
                                next();
                                expr = new WhileNode(cond, body);
                                break;
                        default:
                                expr = parseRank1Expression();
                                break;
                }

                return expr;
        }

        public TypeNode parseTypeExpression() {
                var current = peek(0);
                bool isRef = false;
                int tableDepth = 0;
                ExpressionNode typeId;
                if(current.type == TokenType.OPERATOR_BAND) {
                        isRef = true;
                        next();
                        current = peek(0);
                }
                typeId = parsePrimayExpression();
                current = peek(0);
                while(current.type == TokenType.SQUARE_BRACKET_OPEN) {
                        tableDepth++;
                        next();
                        current = peek(0);
                        if(current.type != TokenType.SQUARE_BRACKET_CLOSE) this._log.TokenError(current.info, "bad Token");
                        next();
                        current = peek(0);
                }
                return new TypeNode(isRef, typeId, tableDepth);
        }

        public ExpressionNode parseRankAssignExpression(ExpressionNode id) {
                var expr = id;
                var current = peek(0);

                while(current.type == TokenType.OPERATOR_ASSIGN ||
                      current.type == TokenType.OPERATOR_PLUS_EQ ||
                      current.type == TokenType.OPERATOR_MINUS_EQ ||
                      current.type == TokenType.OPERATOR_MULT_EQ ||
                      current.type == TokenType.OPERATOR_DIVIDE_EQ ||
                      current.type == TokenType.OPERATOR_MOD_EQ) {
                        var operator = next();
                        var right = parseConditionExpression();
                        expr = new BinaryOperationNode(expr, operator, right);
                        current = peek(0);
                }
                return expr;
        }

        public ExpressionNode parseConditionExpression() {
                var expr = parseCond2Expression();
                var current = peek(0);

                while(current.type == TokenType.OPERATOR_AND ||
                      current.type == TokenType.OPERATOR_OR) {
                        var operator = next();
                        var right = parseConditionExpression();
                        expr = new BinaryOperationNode(expr, operator, right);
                        current = peek(0);
                }
                return expr;
        }

        public ExpressionNode parseCond2Expression() {
                var expr = parseRank1Expression();
                var current = peek(0);

                while(current.type == TokenType.OPERATOR_EQU ||
                      current.type == TokenType.OPERATOR_NOT_EQ ||
                      current.type == TokenType.OPERATOR_GREATER ||
                      current.type == TokenType.OPERATOR_GEQU ||
                      current.type == TokenType.OPRATOR_LESS ||
                      current.type == TokenType.OPERATOR_LEQU) {
                        var operator = next();
                        var right = parseCond2Expression();
                        expr = new BinaryOperationNode(expr, operator, right);
                        current = peek(0);
                }
                return expr;
        }

        public ExpressionNode parseRank1Expression() {
                var expr = parseRank2Expression();
                var current = peek(0);

                while(current.type == TokenType.OPERATOR_PLUS  ||
                      current.type == TokenType.OPERATOR_MINUS) {
                        var operator = next();
                        var right = parseRank1Expression();
                        expr = new BinaryOperationNode(expr, operator, right);
                        current = peek(0);
                }
                return expr;
        }

        public ExpressionNode parseRank2Expression() {
                var expr = parseArrayExpression();
                var current = peek(0);

                while(current.type == TokenType.OPERATOR_MULT  ||
                      current.type == TokenType.OPERATOR_DIVIDE ||
                      current.type == TokenType.OPERATOR_MOD) {
                        var operator = next();
                        var right = parseRank2Expression();
                        expr = new BinaryOperationNode(expr, operator, right);
                        current = peek(0);
                }
                return expr;
        }

        public ExpressionNode parseArrayExpression() {
                var expr = parseRank3Expression();
                var current = peek(0);
                while(current.type == TokenType.SQUARE_BRACKET_OPEN) {
                        next();
                        var index = parseRank1Expression();
                        current = peek(0);
                        if(current.type != TokenType.SQUARE_BRACKET_CLOSE) this._log.TokenError(current.info, "Error Needing Brackets at the end of array statement");
                        next();
                        expr = new ArrayAccessNode(expr, index);
                }
                return expr;
        }

        public ExpressionNode parseRank3Expression() {
                ExpressionNode expr;
                var current = peek(0);

                switch (current.type) {
                        case TokenType.OPERATOR_NOT:
                        case TokenType.OPERATOR_INC:
                        case TokenType.OPERATOR_DEC:
                        case TokenType.OPERATOR_MINUS:
                                var op = next();
                                expr = parseRank3Expression();
                                expr = new PrefixNode(op, expr);
                                break;
                        case TokenType.PARENTESIS_OPEN:
                                next();
                                expr = parseRank1Expression();
                                var closeP = next();
                                if(closeP.type != TokenType.PARENTESIS_CLOSE) _log.TokenError(closeP.info, @"expected Closed parenthesis but got $(closeP.type)");
                                expr = new ParenthesisNode(expr);
                                break;
                        case TokenType.IDENTIFIER:
                                if(peek(1).type == TokenType.PARENTESIS_OPEN) {
                                        expr = parseFunctionCallExpression();
                                } else {
                                        expr = parsePrimayExpression();
                                }
                                break;
                        case TokenType.LITTERAL_FLOAT:
                        case TokenType.LITTERAL_UINT:
                        case TokenType.LITTERAL_STR:
                        case TokenType.LITTERAL_CHAR:
                                expr = parsePrimayExpression();
                                break;
                        default:
                                _log.TokenError(current.info, @"Bad Token $(current)");
                }
                current = peek(0);
                switch (current.type) {
                        case TokenType.OPERATOR_INC:
                        case TokenType.OPERATOR_DEC:
                                var op = next();
                                expr = new SuffixNode(op, expr);
                                break;
                        default:
                                break;
                }
                return expr;
        }

        public ExpressionNode parseFunctionCallExpression() {
                var current = peek(0);
                ExpressionNode node;
                if(current.type != TokenType.IDENTIFIER) _log.TokenError(current.info, @"Unexpectedi token $(current.type)");
                var id = parsePrimayExpression();
                current = peek(0);
                if(current.type != TokenType.PARENTESIS_OPEN) _log.TokenError(current.info, @"Unexpectedii token $(current.type)");
                next();
                var args = parseFunctionCallArgs();
                current = peek(0);
                if(current.type != TokenType.PARENTESIS_CLOSE) _log.TokenError(current.info, @"Unexpectediii token $(current.type)");
                next();
                node = new FunctionCallNode(id, args);
                return node;
        }

        public ExpressionNode[] parseFunctionCallArgs() {
                var current = peek(0);
                var args = new ExpressionNode[] {};
                if(current.type == TokenType.PARENTESIS_CLOSE) return args;
                while(true) {
                        var arg = parseExpression();
                        args += arg;
                        current = peek(0);
                        if(current.type == TokenType.SPECIAL_COMA) {
                                next();
                                continue;
                        }
                        break;
                }
                return args;
        }

        private ExpressionNode parsePrimayExpression() {
                var number = match(TokenType.LITTERAL_UINT);
                if(number == null) number = match(TokenType.LITTERAL_FLOAT);
                if(number == null) number = match(TokenType.LITTERAL_STR);
                if(number == null) number = match(TokenType.LITTERAL_CHAR);
                if(number == null) {
                        number = match(TokenType.IDENTIFIER);
                        if(number != null) return new IdentifierNode(number);
                }
                if(number == null) {
                        number = match(TokenType.TYPE_UINT8);
                        if(number == null) number = match(TokenType.TYPE_UINT16);
                        if(number == null) number = match(TokenType.TYPE_UINT32);
                        if(number == null) number = match(TokenType.TYPE_UINT64);
                        if(number == null) number = match(TokenType.TYPE_INT8);
                        if(number == null) number = match(TokenType.TYPE_INT16);
                        if(number == null) number = match(TokenType.TYPE_INT32);
                        if(number == null) number = match(TokenType.TYPE_INT64);
                        if(number == null) number = match(TokenType.TYPE_F32);
                        if(number == null) number = match(TokenType.TYPE_F64);
                        if(number == null) number = match(TokenType.TYPE_F128);
                        if(number == null) number = match(TokenType.TYPE_STR);
                        if(number == null) number = match(TokenType.TYPE_CHAR);
                        if(number == null) number = match(TokenType.TYPE_BOOL);
                        if(number == null) number = match(TokenType.TYPE_VOID);
                        if(number != null) return new TypeBaseNode(number);
                }
                if(number == null) this._log.TokenError(peek(0).info, @"Unexpectedm TokenType $(peek(0))");
                if(number.type == TokenType.LITTERAL_CHAR) return new CharLitteralNode(number);
                if(number.type != TokenType.LITTERAL_STR) return new NumberLitteralNode(number);
                else return new StringLitteralNode(number);
        }
}
