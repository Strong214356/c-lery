int run(string []av) throws Error {
        SpawnFlags flags = 0;
        string PWD = Environment.get_current_dir();

        flags = SEARCH_PATH | CHILD_INHERITS_STDIN;
		int status;
		Process.spawn_sync(PWD, av, Environ.get(), flags, null, null, null, out status);
		return status;
}

void printTokens(Token[] tkns) {
        foreach(var t in tkns) {
                print(t.to_string_simple());
        }
}

public string[] lib;

void main(string[] args) {
        if(args.length != 2) return;
        string content;
        try {
                Token[] tokenList = {};
                var file = args[1];
                var exist = FileUtils.get_contents(file, out content);
                if(!exist) return;
                var noComm = removeComments(content);
                var tokens = getTokensList(noComm, file);
                foreach (var t in tokens) {
                        tokenList += t;
                }
                //printTokens(tokenList);
                parse(tokenList);
        } catch (Error e) {
                print ("%s\n", e.message);
        }
}
