int labelId = 1;

public int getNextLabel() {
        return labelId++;
}

public abstract class BaseNode {}

public class IntLitteralNode : BaseNode {
        public int val;

        public IntLitteralNode(int val) {
                this.val = val;
        }

        public static IntLitteralNode? parse(Token[] toks, int pos) {
                if(toks[pos].type != TokenType.LITTERAL_UINT) return null;
                return new IntLitteralNode(int.parse(toks[pos].text));
        }
}

BaseNode parsePrimary(Token[] toks, ref int pos) {
        BaseNode? ret = IntLitteralNode.parse(toks, pos);
        if(ret == null) ret = IdentifierNode.parse(toks, pos);
        if(ret == null) error("unable to parse primary expression");
        pos++;
        return ret ?? new IntLitteralNode(0);
}

string getOperator(TokenType type) {
        switch(type) {
                case TokenType.OPERATOR_PLUS:
                        return "+";
                case TokenType.OPERATOR_MINUS:
                        return "-";
                case TokenType.OPERATOR_MULT:
                        return "*";
                case TokenType.OPERATOR_DIVIDE:
                        return "/";
                case TokenType.OPERATOR_EQU:
                        return "==";
                case TokenType.OPERATOR_NOT_EQ:
                        return "!=";
                case TokenType.OPRATOR_LESS:
                        return "<";
                case TokenType.OPERATOR_GREATER:
                        return ">";
                case TokenType.OPERATOR_LEQU:
                        return "<=";
                case TokenType.OPERATOR_GEQU:
                        return ">=";
                default:
                        return "";
        }
}

public bool isConditionOp(string op) {
        switch(op) {
                case "==":
                case "!=":
                case "<":
                case ">":
                case "<=":
                case ">=":
                        return true;
        }
        return false;
}

public class BinaryOperatorNode : BaseNode {
        public BaseNode left;
        public BaseNode right;
        public string operator;
        public bool parentIsJump;

        public BinaryOperatorNode(string operator, BaseNode left, BaseNode right) {
                this.left = left;
                this.right = right;
                this.operator = operator;
                this.parentIsJump = false;
        }

        public static BaseNode parseBase(Token[] toks, ref int i) {
                return BinaryOperatorNode.parseCondL1(toks, ref i);
        }

        public static BaseNode parseCondL1(Token[] toks, ref int i) {
               BaseNode left = parseCondL2(toks, ref i); 
               while(toks[i].type == TokenType.OPRATOR_LESS
                     || toks[i].type == TokenType.OPERATOR_GREATER
                     || toks[i].type == TokenType.OPERATOR_LEQU
                     || toks[i].type == TokenType.OPERATOR_GEQU) {
                        string c = getOperator(toks[i].type);
                        i++;
                        left = new BinaryOperatorNode(c, left, parseCondL2(toks, ref i));
                }
                return left;
        }
        public static BaseNode parseCondL2(Token[] toks, ref int i) {
               BaseNode left = parseL1(toks, ref i); 
               while(toks[i].type == TokenType.OPERATOR_EQU
                     || toks[i].type == TokenType.OPERATOR_NOT_EQ) {
                        string c = getOperator(toks[i].type);
                        i++;
                        left = new BinaryOperatorNode(c, left, parseL1(toks, ref i));
                }
                return left;
        }

        public static BaseNode parseL1(Token[] toks, ref int i) {
               BaseNode left = parseL2(toks, ref i); 
               while(toks[i].type == TokenType.OPERATOR_PLUS
                     || toks[i].type == TokenType.OPERATOR_MINUS) {
                        string c = getOperator(toks[i].type);
                        i++;
                        left = new BinaryOperatorNode(c, left, parseL2(toks, ref i));
                }
                return left;
        }

        public static BaseNode parseL2(Token[] toks, ref int i) {
               BaseNode left = parsePrimary(toks, ref i); 
               while(toks[i].type == TokenType.OPERATOR_MULT
                     || toks[i].type == TokenType.OPERATOR_DIVIDE) {
                        string c = getOperator(toks[i].type);
                        i++;
                        left = new BinaryOperatorNode(c, left, parsePrimary(toks, ref i));
                }
                return left;
        }
}

public class assignOperatorNode : BaseNode {
        public BaseNode left;
        public string id;

        public assignOperatorNode(string id, BaseNode left) {
                this.id = id;
                this.left = left;
        }
}

public class VarCreateNode : BaseNode {
        public VarCreateNode() {

        }
}

public class IdentifierNode : BaseNode {
        public string val;

        public IdentifierNode(string val) {
                this.val = val;
        }

        public static IdentifierNode? parse(Token[] toks, int pos) {
                if(toks[pos].type != TokenType.IDENTIFIER) return null;
                return new IdentifierNode(toks[pos].text);
        }
}

public class BlockNode : BaseNode {
        BaseNode[] statements;

        public BlockNode() {
                this.statements = {};
        }

        public void addNode(BaseNode n) {
                this.statements += n;
        }

        public BaseNode[] getNodes() {
                return this.statements;
        }
}

public class ReturnNode : BaseNode {
        public BaseNode expr;

        public ReturnNode(BaseNode expr) {
                this.expr = expr;
        }
}

public class IfNode : BaseNode {
        public BaseNode cond;
        public BaseNode ifBlock;
        public BaseNode? elseBlock;
        
        public IfNode(BaseNode cond, BaseNode ifBlock, BaseNode? elseBlock) {
                this.cond = cond;
                this.ifBlock = ifBlock;
                this.elseBlock = elseBlock;
        }
}

public class WhileNode : BaseNode {
        public BaseNode cond;
        public BaseNode block;

        public WhileNode(BaseNode cond, BaseNode block) {
                this.cond = cond;
                this.block = block;
        }
}

public class ForNode : BaseNode {
        public BaseNode preop;
        public BaseNode cond;
        public BaseNode postop;
        public BaseNode block;

        public ForNode(BaseNode preop, BaseNode cond, BaseNode postop, BaseNode block) {
                this.preop = preop;
                this.cond = cond;
                this.postop = postop;
                this.block = block;
        }
}

public class FunctionNode : BaseNode {
        public string name;
        public BaseNode block;

        public FunctionNode(string s, BaseNode block) {
                this.name = s;
                this.block = block;
        }
}

public ReturnNode parseReturn(Token[] toks, ref int i) {
       if(toks[i].type != TokenType.KW_RETURN) error("Excpected statement");
       i++;
       BaseNode n = BinaryOperatorNode.parseBase(toks, ref i);
       return new ReturnNode(n);
}

public assignOperatorNode parseAssign(Token id, Token[] toks, ref int i) {
        int sid = findSym(id.text ?? "unreachable");
        if(sid == -1) error(@"undefined symbol $(id.text ?? "unreachable")");
        if(toks[i].type != TokenType.OPERATOR_ASSIGN) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode expr = BinaryOperatorNode.parseBase(toks, ref i);
        return new assignOperatorNode(id.text ?? "unreachable", expr);
}

public WhileNode parseWhile(Token[] toks, ref int i) {
        if(toks[i].type != TokenType.KW_WHILE) error(@"unexpected token $(toks[i].type)");
        i++;
        if(toks[i].type != TokenType.PARENTESIS_OPEN) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode cond = BinaryOperatorNode.parseBase(toks, ref i);
        if(cond is BinaryOperatorNode) {
                ((BinaryOperatorNode)(cond as BinaryOperatorNode)).parentIsJump = true;
                if(!isConditionOp(((BinaryOperatorNode)(cond as BinaryOperatorNode)).operator)) error("unalbe to jump with arithemtic operation");
        }
        if(toks[i].type != TokenType.PARENTESIS_CLOSE) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode block = parseBlock(toks, ref i);
        return new WhileNode(cond, block);
}

public IfNode parseIf(Token[] toks, ref int i) {
        if(toks[i].type != TokenType.KW_IF) error(@"unexpected token $(toks[i].type)");
        i++;
        if(toks[i].type != TokenType.PARENTESIS_OPEN) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode cond = BinaryOperatorNode.parseBase(toks, ref i);
        if(cond is BinaryOperatorNode) {
                ((BinaryOperatorNode)(cond as BinaryOperatorNode)).parentIsJump = true;
                if(!isConditionOp(((BinaryOperatorNode)(cond as BinaryOperatorNode)).operator)) error("unalbe to jump with arithemtic operation");
        }
        if(toks[i].type != TokenType.PARENTESIS_CLOSE) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode ifBlock = parseBlock(toks, ref i);
        BaseNode? elseBlock = null;
        if(toks[i].type == TokenType.KW_ELSE) {
                i++;
                elseBlock = parseBlock(toks, ref i);
        }
        return new IfNode(cond, ifBlock, elseBlock);
}

public ForNode parseFor(Token[] toks, ref int i) {
        if(toks[i].type != TokenType.KW_FOR) error(@"unexpected token $(toks[i].type)");
        i++;
        if(toks[i].type != TokenType.PARENTESIS_OPEN) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode preop = singleStatement(toks, ref i);
        if(needSemi(preop) && toks[i].type != TokenType.SPECIAL_SEMICOLON) error("Excpected semicolon");
        if(needSemi(preop)) i++;
        BaseNode cond = BinaryOperatorNode.parseBase(toks, ref i);
        if(cond is BinaryOperatorNode) {
                ((BinaryOperatorNode)(cond as BinaryOperatorNode)).parentIsJump = true;
                if(!isConditionOp(((BinaryOperatorNode)(cond as BinaryOperatorNode)).operator)) error("unalbe to jump with arithemtic operation");
        }
        if(toks[i].type != TokenType.SPECIAL_SEMICOLON) error("Excpected semicolon");
        i++;
        BaseNode postop = singleStatement(toks, ref i);
        if(toks[i].type != TokenType.PARENTESIS_CLOSE) error(@"unexpected token $(toks[i].type)");
        i++;
        BaseNode block = parseBlock(toks, ref i);
        return new ForNode(preop, cond, postop, block);
}

struct symtable {
        public string name;

        public symtable(string name) {
                this.name = name;
        }
}

symtable[] Gtable;

int findSym(string name) {
        for(int i = 0; i < Gtable.length; i++) {
                if(Gtable[i].name == name) return i;
        }
        return -1;
}

int addsym(string name) {
        int y;
        if((y = findSym(name)) != -1) return y;
        Gtable += symtable(name);
        return Gtable.length - 1;
}

public VarCreateNode parseVarCreate(Token id, Token[] toks, ref int i) {
        i++;
        if(toks[i].type != TokenType.TYPE_UINT32) error(@"unexpected token $(toks[i].type)"); 
        i++;
        addsym(id.text ?? "unreachable");
        gensym(id.text ?? "unreachable");
        return new VarCreateNode();
}

public bool needSemi(BaseNode n) {
        if(n is ReturnNode) return true;
        if(n is assignOperatorNode) return true;
        if(n is VarCreateNode) return true;
        return false;
}

public BaseNode singleStatement(Token[] toks, ref int i) {
        BaseNode n;
        switch(toks[i].type) {
                case TokenType.KW_RETURN:
                        n = parseReturn(toks, ref i);
                        break;
                case TokenType.IDENTIFIER:
                        var id = toks[i];
                        i++;
                        if(toks[i].type == TokenType.OPERATOR_ASSIGN) n = parseAssign(id, toks, ref i);
                        else if(toks[i].type == TokenType.SPECIAL_VAR_TYPE) n = parseVarCreate(id, toks, ref i);
                        else {
                                TokenInfo inf = toks[i].info;
                                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
                        }
                        break;
                case TokenType.KW_IF:
                        n = parseIf(toks, ref i);
                        break;
                case TokenType.KW_WHILE:
                        n = parseWhile(toks, ref i);
                        break;
                case TokenType.KW_FOR:
                        n = parseFor(toks, ref i);
                        break;
                default:
                        TokenInfo inf = toks[i].info;
                        error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        return n;
}

public BlockNode parseBlock(Token[] toks, ref int i) {
        BlockNode block = new BlockNode();
        if(toks[i].type != TokenType.BRACKET_OPEN) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        i++;
        while(true) {
                if(toks[i].type == TokenType.BRACKET_CLOSE) {
                        i++;
                        break;
                }
                BaseNode n = singleStatement(toks, ref i);
                if(needSemi(n) && toks[i].type != TokenType.SPECIAL_SEMICOLON) error("Excpected semicolon");
                if(needSemi(n)) i++;
                if(!(n is VarCreateNode)) block.addNode(n);
                freeall_register();
        }
        return block;
}

public FunctionNode parseFunction(Token[] toks, ref int i) {
        if(toks[i].type != TokenType.KW_FUNCTION) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        i++;
        if(toks[i].type != TokenType.IDENTIFIER) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        string name = toks[i].text ?? "unreachable";
        addsym(toks[i].text ?? "unreachable");
        i++;
        if(toks[i].type != TokenType.PARENTESIS_OPEN) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        i++;
        if(toks[i].type != TokenType.PARENTESIS_CLOSE) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        i++;
        if(toks[i].type != TokenType.SPECIAL_RETURN_TYPE) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        i++;
        if(toks[i].type != TokenType.TYPE_VOID) {
                TokenInfo inf = toks[i].info;
                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
        }
        i++;
        BaseNode n = parseBlock(toks, ref i);
        return new FunctionNode(name, n);
}

public void parse(Token[] toks) {
        Gtable = {};
        int i = 0;
        cgpreamble();
        while(true) {
                switch(toks[i].type) {
                        case TokenType.KW_FUNCTION:
                                gen(parseFunction(toks, ref i), 0);
                                break;
                        case TokenType.EOC:
                                return;
                        default:
                                TokenInfo inf = toks[i].info;
                                error(@"\nunexpected token $(toks[i].type) => $(inf.file):$(inf.line):$(inf.column)");
                }
        }
}
